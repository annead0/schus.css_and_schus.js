var video = document.getElementById("backgroundvideo");
var btn = document.getElementById("buttontopause");
function pauseVid() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}